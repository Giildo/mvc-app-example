<?php

class Database
{
    /**
     * @var PDO
     */
    private PDO $pdo;
    /**
     * @var Exception|PDOException
     */
    private $error;

    /**
     * ORM constructor.
     *
     * @param string      $dbname
     * @param string      $username
     * @param string      $password
     * @param string|null $host
     */
    public function __construct(
        string $dbname,
        string $username,
        string $password,
        ?string $host = 'localhost'
    ) {
        try {
            $this->pdo = new PDO("pgsql:host={$host};port=5432;dbname={$dbname};user={$username};password={$password}");
            // Pour l'utilisation de fetchAll/fetch avec des object construits
            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            // Pour que PDO renvoie même les exceptions
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // SERIAL = INTEGER AUTO_INCREMENT en postgresql
            $this->pdo->query('CREATE TABLE IF NOT EXISTS user_cours (
            id SERIAL NOT NULL,
            username VARCHAR(100),
            password VARCHAR(50)
            )');
        } catch (PDOException $exception) {
            $this->error = $exception;
        }
    }

    /**
     * @return Exception|PDOException
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }
}