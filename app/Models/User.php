<?php

class User
{
    private ?int $id;
    private ?string $username;
    private ?string $password;

    /**
     * User constructor.
     *
     * @param int|null    $id
     * @param string|null $username
     * @param string|null $password
     */
    public function __construct(?int $id, ?string $username, ?string $password)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function createUser(string $username, string $password)
    {
        $username = htmlspecialchars(trim($username));
        if (strlen($username) <= 100 && $username !== '') {
            $this->username = $username;
        }

        $this->password = password_hash($password, PASSWORD_ARGON2ID);
    }
}