<?php

class Server
{
    /**
     * @var string
     */
    private string $method;
    /**
     * URI = /chemin/apres/server
     *
     * @var string
     */
    private string $uri;
    /**
     * Données envoyées via le formulaire
     *
     * @var mixed
     */
    private $data;

    /**
     * Server constructor.
     */
    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        // split pour enlever les query param
        $this->uri = preg_split('#\?#', $_SERVER['REQUEST_URI'])[0];
        if ($this->method === 'POST') {
            $this->data = $_POST;
        } else {
            $this->data = $_GET;
        }
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }
}