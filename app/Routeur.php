<?php

include_once 'Server.php';
include_once 'Controllers.php';

class Routeur
{
    /**
     * @var array
     */
    private array $routes;
    /**
     * Le préfixe permet de dire que c'est un objet instancié depuis la classe Server
     *
     * @var Server
     */
    private Server $server;
    /**
     * @var Controllers
     */
    private Controllers $controllers;

    /**
     * Routeur constructor.
     *
     * @param array $routes
     */
    public function __construct(array $routes)
    {
        $this->routes = $routes;
        $this->server = new Server();
        $this->controllers = new Controllers();
    }

    public function router()
    {
        if (key_exists($this->server->getUri(), $this->routes)) {
            $controllerName = $this->routes[$this->server->getUri()];
            return $this->controllers->$controllerName($this->server);
        } else {
            // Envoie un code 404
            http_response_code(404);
            ob_start();
            include_once 'Views/404.php';
            return ob_get_clean();
        }
    }
}