<?php

include_once 'Database.php';
include_once 'Models/User.php';

class Controllers
{
    private Database $database;
    private UserRepository $userRepository;

    /**
     * Controllers constructor.
     */
    public function __construct()
    {
        $this->database = new Database(
            'cours_jonathan',
            'giildo',
            getenv('PASSWORD')
        );
        $this->userRepository = new UserRepository($this->database);
    }

    public function utilisateurDetail(Server $server)
    {

        $utilisateur = $this->userRepository->getUtilisateur($server->getData()['id']);

        //Deux solutions
        // 1. appel AJAX -> echo json_encode($utilisateur);
        // 2. cf. suite du code
        // Permet de créer un environement local
        ob_start(); // débute l'environement local
        include_once 'Views/UtilisateurDetail.php';
        $view = ob_get_clean(); //ferme l'environement et retourne tout ce qui a été fait à l'intérieur dans la variable $view

        return $view;
    }

    public function creerUtilisateur(Server $server)
    {
        if ($server->getMethod() === 'post' && !empty($server->getData())) {
            $this->database->creerUtilisateur($server->getData());
        }

        ob_start();
        include_once 'Views/CreerUtilisateur.php';
        return ob_get_clean();
    }
}