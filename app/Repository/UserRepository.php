<?php

include "../Database.php";
include "../Models/User.php";

class UserRepository
{
    private Database $database;

    /**
     * UserRepository constructor.
     *
     * @param Database $database
     */
    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function getUtilisateur(int $utilisateurId): User
    {
//        return $this->pdo->query("SELECT * FROM user_cours WHERE id='" . $utilisateurId . "'");
        /** @var stdClass $user */
        $user = $this->database->getPdo()
                               ->query("SELECT * FROM user_cours WHERE id='{$utilisateurId}'")
                               ->fetch();

        return new User(
            $user->id,
            $user->username,
            $user->password
        );
    }
}