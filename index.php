<?php
include_once 'app/Routeur.php';

// 'uri' => 'nom du controller'
$routes = [
    '/utilisateur' => 'utilisateurDetail',
    '/creer-utilisateur' => 'creerUtilisateur',
];

$routeur = new Routeur($routes);

echo $routeur->router();
